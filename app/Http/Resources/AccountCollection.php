<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AccountCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->resource->toArray();
        $links = [
            'path' => $this->resource->path(),
            'firstPageUrl' => $data['first_page_url'],
            'lastPageUrl' =>  $data['last_page_url'],
            'nextPageUrl' => $data['next_page_url'],
            'prevPageUrl' => $data['prev_page_url'],
        ];
        return [
            'data' => $this->collection,
            'links' => $links,
            'meta' =>   [
                'currentPage' => $data['current_page'],
                'from' => $data['from'],
                'lastPage' => $data['last_page'],
                'perPage' => $data['per_page'],
                'to' => $data['to'],
                'total' => $data['total'],
                'count' => $data['per_page'],
            ]
        ];
    }
    public function toResponse($request)
    {
        return JsonResource::toResponse($request);
    }
}
