<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('client_id');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');

            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('email', 150)->unique()->index()->comment('User unique email address.');
            $table->string('password', 256);
            $table->string('phone', 20);
            $table->string('profile_url', 255)->nullable();
            $table->enum('status', ['Active', 'Inactive'])->comment('User status Active|Inactive');
            $table->timestamp('last_password_reset')->default(now())->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
