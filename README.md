# POC - Client Account Self Registration

![https://laravel.com](https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg)

![Build Status](https://travis-ci.org/laravel/framework.svg)
![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)
![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)
![License](https://img.shields.io/packagist/l/laravel/framework)

### About Project
---

Laravel Test Project For Client Account Self Registration 

- Follow the installation steps to up and running.


## Installation Process
---

### Clone the BitBucket Repo.:
```
git clone https://vikasukani@bitbucket.org/vikasukani/laravel-test.git
```

### Install composer packages: 
```
composer install

```
### Copy .env from .env.example and set Google Map API Key: 
- Add api token in variable `GOOGLE_MAPS_GEOCODING_API_KEY` in `.env` file
- Setup database credentials
```
cp .env.example .env

```

### Generate Keys
```
php artisan key:generate

```

### Run Migrations
```
php artisan migrate

```
### Run Server
```
php artisan serve

```


# API EndPoints:

###  **POST**: `/api/register` => To create an  client with user details.
1. Payload Data (JSON PostMan)
```
{
    "name": "MyName",
    "email": "isMyMail@emasil.com",
    "address1": "Yogichowck So.",
    "address2": "Yogichowck Circle",
    "city": "Surat",
    "state": "Gujarat",
    "country": "India",
    "zipCode": 395006,
    "phoneNo1": 7374747474,
    "user": {
        "firstName": "My Name",
        "lastName": "Is Awesome",
        "email": "isMailName@gmail.com",
        "password": "UkaniPassword123",
        "passwordConfirmation": "UkaniPassword123",
        "profile_url": "http://mvix-test.local/api/register",
        "status": "Active",
        "phone": 7335437434
    }
}
```
- Success Response Data (JSON PostMan)

```
{
    "statusCode": 201,
    "message": "Client has been successfully registered.",
    "data": {
        "id": 5,
        "client_name": "Ukani",
        "email": "isMyMail4@emasil.com",
        "address1": "Near Punagam",
        "address2": "Near Punagam",
        "city": "Surat",
        "state": "Gujarat",
        "country": "India",
        "latitude": 21.1820786,
        "longitude": 72.8405329,
        "phoneNo1": 7374747474,
        "phoneNo2": null,
        "zip": "6060",
        "start_validity": "2021-07-16T07:39:44.928868Z",
        "end_validity": "2021-07-31T07:39:44.928962Z",
        "status": "Active",
        "created_at": "2021-07-16T06:26:03.000000Z",
        "updated_at": "2021-07-16T07:39:44.000000Z",
        "deleted_at": null,
        "totalUser": {
            "all": 8,
            "active": 8,
            "inactive": 0
        }
    }
}
```

### **GET**: `/api/account` => To Fetch all client details.
- Query String data (JSON PostMan)
- Example QueryString URL: `http://mvix-test.local/api/account?client_name=Ukani&sort=client_name:asc`

```
client_name=Ukani // Filter criteria ( dbKeys = 'Value string')
sort=client_name:asc //  Sorting criteria ( key =  dbKeys:asc|desc)
```

- Response Data (JSON PostMan)
```
{
    "statusCode": 200,
    "message": "Accounts details retrieved.",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 16,
                "client_name": "Ukani",
                "address1": "Near Punagam",
                "address2": "Near Punagam",
                "city": "Surat",
                "state": "Gujarat",
                "country": "India",
                "latitude": 21.1820786,
                "longitude": 72.8405329,
                "phoneNo1": "7374747474",
                "phoneNo2": null,
                "zipCode": "395010",
                "start_validity": "2021-07-16",
                "end_validity": "2021-07-31",
                "status": "Active",
                "created_at": "2021-07-16T05:19:21.000000Z",
                "updated_at": "2021-07-16T05:27:37.000000Z",
                "deleted_at": null,
                "totalUser": {
                    "all": 5,
                    "active": 4,
                    "inactive": 1
                }
            }
        ],
        "first_page_url": "http://mvix-test.local/api/account?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://mvix-test.local/api/account?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://mvix-test.local/api/account?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://mvix-test.local/api/account",
        "per_page": 5,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```

## Run Tests
```
php artisan test

```
