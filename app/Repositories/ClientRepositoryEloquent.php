<?php

namespace App\Repositories;

use App\Models\Client;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Auth;

class ClientRepositoryEloquent extends BaseRepository implements UsersRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Client::class;
    }

    /**
     * boot => Boot up the repository, pushing criteria
     *
     * @return void
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * commonFilterFn => make common filter for list and getDetailsByInput
     *
     * @param  mixed $value
     * @param  mixed $input
     *
     * @return void
     */
    protected function commonFilterFn(&$value, $input)
    {

        /** check names */
        if (isset($input['client_name'])) {
            $value = $value->where('client_name', $input['client_name']);
        }
        if (isset($input['city'])) {
            $value = $value->where('city', $input['city']);
        }

        /** date wise records */
        if (isset($input['start_date'])) {
            $value = $value->where('created_at', ">=", $input['start_date']);
        }

        if (isset($input['latitude'])) {
            $value = $value->where('latitude', $input['latitude']);
        }
        if (isset($input['longitude'])) {
            $value = $value->where('longitude', $input['longitude']);
        }
    }

    protected function commonSortingFn(&$value, $input) {
        if (isset($input['sort'])) {
            $sortArr = explode(':', $input['sort']);
            $value = $value->orderBy($sortArr[0], $sortArr[1]);
        }
    }

    /**
     * getDetails => get details for listing
     *
     * @param  mixed $input
     *
     * @return void
     */
    public function getDetails($input = null)
    {
        $value = $this->makeModel();

        // common filter applied here
        $this->commonFilterFn($value, $input);

        /** Common sorting records */
        $this->commonSortingFn($value, $input);

        // $count = $value->count();
        $input['perPage'] = $input['perPage'] ?? 10; // If perpage not found in request then set default to 5
        return $value->paginate($input['perPage']);
    }
}
