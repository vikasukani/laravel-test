<?php

use App\Http\Controllers\v1\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// 
Route::post('/register', [RegisterController::class, 'register']);
Route::get('/account', [RegisterController::class, 'account']);

Route::get('/test-geo', function () {
    //  . env("GOOGLE_MAPS_GEOCODING_API_KEY")
    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=395006&sensor=false&key=AIzaSyA3vAIJXj84duSSE-qCjLiC9a6s7paWKt4";
    $result_string = file_get_contents($url);
    return json_decode($result_string, true);
});