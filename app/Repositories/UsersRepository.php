<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsersRepository.
 *
 * @package namespace App\Libraries\RepositoriesInterfaces;
 */
interface UsersRepository extends RepositoryInterface
{
    //
}
