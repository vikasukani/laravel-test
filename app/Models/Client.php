<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Validator;
use App\Models\User;



class Client extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_name',
        'email',
        'address1',
        'address2',
        'city',
        'state',
        'country',
        'latitude',
        'longitude',
        'phoneNo1',
        'phoneNo2',
        'zip',
        'start_validity',
        'end_validity',
        'status',
    ];

    /** Adding Customize fields while fetching data. */
    protected $appends = ['totalUser'];

    /** Get Customized Attribute for Total user */
    public function getTotalUserAttribute($q)
    {
        $allData =  [
            'all' => User::where('client_id', $this->id)->count(),
            'active' => User::where('client_id', $this->id)->where('status', 'Active')->count(),
            'inactive' => User::where('client_id', $this->id)->where('status', 'Inactive')->count(),
        ];
        return $this->attributes['totalUser'] = $allData;
    }

    /**
     * rules => set Validation Rules
     *
     * @param  mixed $id
     *
     * @return array
     */
    public static function rules($id)
    {
        $once = isset($id) ? 'sometimes|' : '';
        return [
            'name' => $once . 'required|min:2|max:100',
            'email' => $once . "required|email", // Should not be unique, Coz email using for finding clients if already exists
            'address1' => $once . 'required|min:3',
            'address2' => $once . 'required',
            'city' => $once . 'required',
            'state' => $once . 'required',
            'country' => $once . 'required',
            'zipCode' => $once . 'required|integer|digits_between:4,6',
            'phoneNo1' => $once . 'required|integer', // min:10|max:12
            /** User Validation */
            'user.firstName' => $once . 'required|min:3|max:255',
            'user.lastName' => $once . 'required|min:3|max:255',
            'user.email' => $once . "required|email|unique:App\Models\User,email,{$id}|max:255",
            'user.password' => $once . 'required|min:6|same:user.passwordConfirmation|regex:/(^([a-zA-z]+)(\d+)?$)/u',
            'user.passwordConfirmation' => $once . 'required|max:255',
            'user.phone' => $once . 'required|integer' // min:10|max:12
        ];
    }

    /**
     * messages => Set Error Message
     *
     * @return array
     */
    public static function messages()
    {
        /** setup custom error message in trans files */
        return [
            // 'required' => __('validation.required'),
        ];
    }

    /**
     * validation => **
     *
     *
     * @param  mixed $input
     * @param  mixed $id
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validation($input, $id = null)
    {
        return Validator::make($input, Client::rules($id), Client::messages());
    }

    /**
     * Get all of the users for the Client
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'client_id', 'id');
    }
}
