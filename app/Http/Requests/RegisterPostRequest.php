<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /** Client Validation */
            'name' => 'required',
            'address1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'zipCode' => 'required',
            'phoneNo1' => 'required',
            /** User Validation */
            'user.firstName' => 'required|unique:posts|max:255',
            'user.lastName' => 'required|unique:posts|max:255',
            'user.email' => 'required|unique:posts|max:255',
            'user.password' => 'required|unique:posts|max:255',
            'user.passwordConfirmation' => 'required|unique:posts|max:255',
            'user.phone' => 'required|unique:posts|max:255'
        ];
    }
}
