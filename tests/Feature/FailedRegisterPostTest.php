<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FailedRegisterPostTest extends TestCase
{
    
    /**
     * A Failed Test for registering Client details.
     *
     * @return void
     */
    public function test_failed_register_client_with_user_example()
    {
        $response = $this->postJson('/api/register', [
            "name" => "Failed Test Case User"
        ]);

        $response->assertStatus(422);
    }
}
