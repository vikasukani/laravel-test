<?php

namespace App\Support;

use Illuminate\Support\Facades\Redis;

trait MessageTrait
{

    /**
     * Making Common Response
     * return JSON|ARRAY
     */
    public function sendResponse($data = null, $message, $code = 200)
    {
        return response()->json([
            'statusCode' => $code,
            "message" => $message,
            "data" => $data
        ], $code);
    }


    /**
     * Method getGEOMapDataByAddress
     *
     * @param $address $address
     * @param $zipCode $zipCode
     *
     * @return void
     */
    public function getGEOMapDataByAddress($address, $zipCode)
    {
        try {
            $cachedData = Redis::get('zipCode_' . $zipCode);
            $cachedLatLong = json_decode($cachedData, true);
            if (isset($cachedData) && $cachedLatLong['latitude']) {
                $latLogArr = [
                    'latitude' => $cachedLatLong['latitude'], // If not found in any case bypass it. (Testing only)
                    'longitude' => $cachedLatLong['longitude'],   // If not found in any case bypass it. (Testing only)
                ];
            } else {
                $input = $address ?? $zipCode;
                // https://maps.googleapis.com/maps/api/geocode/json?address=395006&key=GOOGLE_MAPS_GEOCODING_API_KEY
                $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode((string) $input) . "&key=" . env("GOOGLE_MAPS_GEOCODING_API_KEY");
                // $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . (string) $zipCode . "&key=" . env("GOOGLE_MAPS_GEOCODING_API_KEY");
                $result_string = file_get_contents($url);
                $geoData = json_decode($result_string, true);
                if ($geoData['status'] == "REQUEST_DENIED") {
                    return [
                        'flag' => false, // FIXME -: Default: False || Set this as true to byPass the G-Map error response
                        'data' => null,
                        'message' => __($geoData['error_message'])
                    ];
                }
                $latLogArr = [
                    'latitude' => $geoData['results'][0]['geometry']['location']['lat'] ?? 21.1820786, // If not found in any case bypass it. (Testing only)
                    'longitude' => $geoData['results'][0]['geometry']['location']['lng'] ?? 72.8405329, // If not found in any case bypass it. (Testing only)
                ];
                Redis::set('zipCode_' . (string) $zipCode, json_encode($latLogArr));
            }
            $latLogArr['latitude'];
            $latLogArr['longitude'];
            return [
                'flag' => true,
                'data' => $latLogArr
            ];
        } catch (\Exception $exception) {
            return [
                'flag' => false,
                'data' => null,
                'message' => __($exception->getMessage())
            ];
        }
    }
}
