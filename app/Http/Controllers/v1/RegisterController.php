<?php

namespace App\Http\Controllers\v1;

use Carbon\Carbon;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Support\MessageTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\AccountCollection;
use Illuminate\Support\Facades\Hash;
use App\Repositories\UsersRepositoryEloquent;
use App\Repositories\ClientRepositoryEloquent;

class RegisterController extends Controller
{
    use MessageTrait;

    protected $userRepository;
    protected $clientRepository;

    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(
        UsersRepositoryEloquent $userRepository,
        ClientRepositoryEloquent $clientRepository
    ) {
        $this->userRepository = $userRepository;
        $this->clientRepository = $clientRepository;
    }

    /**
     * Fetch All Clients Details
     *
     * @param Request $request 
     *
     * @return void|object
     */
    public function account(Request $request)
    {
        $input = $request->all();

        /** fetch Filter and sorting wise client details  */
        return new AccountCollection($this->clientRepository->getDetails($input));
    }

    /**
     * Registering Client with user details
     *
     * @param Request $request 
     *
     * @return void|object
     */
    public function register(Request $request)
    {
        $input = $request->all();

        /** Setup validation */
        $validation =  Client::validation($input);
        if (isset($validation) && $validation->errors()->count() > 0) {
            return $this->sendResponse(null, $validation->errors()->all(), 422);
        }

        /** REDIS Implementation ... */
        /** Get LatLong by PostCode from GMap APi*/
        $latLogArrRes = $this->getGEOMapDataByAddress($input['address1'], $input['zipCode']);
        if ($latLogArrRes['flag'] == false) {
            return $this->sendResponse(null, $latLogArrRes['message'], 422);
        }
        /** REDIS Implementation END Here ... */
        $input['latitude'] = $latLogArrRes['data']['latitude'] ?? 21.1820786; // If not found in any case bypass it. (Testing only)
        $input['longitude'] = $latLogArrRes['data']['longitude'] ?? 72.8405329; // If not found in any case bypass it. (Testing only)
        $input['start_validity'] = Carbon::now(); // Set Current UTC Date
        $input['end_validity'] = Carbon::now()->addDays(15);  // Set and Add 15 Days in Current Date
        $input['status'] = $input['status'] ?? 'Active';

        /** Saving data */
        $clientData = $input;
        $clientData['client_name'] = $input['name'];
        $clientData['address1'] = $input['address2'] ?? '';
        $clientData['address2'] = $input['address2'] ?? '';
        $clientData['zip'] = $input['zipCode'] ?? '';

        /** Creating Client Data */
        $createdClient = $this->clientRepository->updateOrCreate(['email' => $input['email']], $clientData);
        // $createdClient = $this->clientRepository->create($clientData);

        /** check if client created */
        if (isset($createdClient, $createdClient->id)) {
            /** Updating Data */
            $user = $input['user'];
            $user['first_name'] = $user['firstName'];
            $user['last_name'] = $user['lastName'];

            /** Check if client already exist */
            $user['client_id'] = $createdClient->id;
            $user['password'] = Hash::make($user['password']);
            $this->userRepository->create($user);
            return $this->sendResponse($createdClient, __("Client has been successfully registered."), 201);
        } else {
            return $this->sendResponse(null, __("There is some issue while creating client."), 422);
        }
    }
}
