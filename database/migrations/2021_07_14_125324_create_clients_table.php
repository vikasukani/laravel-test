<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();

            $table->string('client_name', 100);
            $table->string('email', 150)->index()->comment('Client email for checking already there or not.');
            $table->text('address1');
            $table->text('address2')->nullable();
            $table->string('city', 100);
            $table->string('state', 100);
            $table->string('country', 100);
            $table->double('latitude')->comment('Fetch from Google Map API');
            $table->double('longitude')->comment('Fetch from Google Map API');
            $table->string('phoneNo1', 20);
            $table->string('phoneNo2', 20)->nullable();
            $table->string('zip', 20);
            $table->date('start_validity');
            $table->date('end_validity');
            $table->enum('status', ['Active', 'Inactive'])->comment('User status Active|Inactive');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
