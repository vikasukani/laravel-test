<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterClientTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_register_client_with_user_example()
    {
        $response = $this->postJson('/api/register', [
            "name" => "Test Case User",
            "email" => "newEmail@gmail.com",
            "address1" => "Test Address 1",
            "address2" => "Test Address 2",
            "city" => "Test City",
            "state" => "Test State",
            "country" => "Test Country",
            "zipCode" => 395006,
            "phoneNo1" => 9909650780,
            "user" => [
                "firstName" => "Test first name",
                "lastName" => "Test last name",
                "email" => uniqid() . "_TestEmail@email.com",
                "password" => "TestPassword123",
                "passwordConfirmation" => "TestPassword123",
                "phone" => 99085485642
            ]
        ]);
        $response->assertStatus(201);
    }
}
