<?php

namespace App\Repositories;

use App\Models\User;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Auth;

class UsersRepositoryEloquent extends BaseRepository implements UsersRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * boot => Boot up the repository, pushing criteria
     *
     * @return void
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * commonFilterFn => make common filter for list and getDetailsByInput
     *
     * @param  mixed $value
     * @param  mixed $input
     *
     * @return void
     */
    protected function commonFilterFn(&$value, $input)
    {

        /** check names */
        if (isset($input['name'])) {
            $value = $value->whereName($input['name']);
        }

        /** check email  */
        if (isset($input['email'])) {
            $value = $value->whereEmail($input['email']);
        }

        // added relation here.
        // $value = $value->

        /** date wise records */
        if (isset($input['start_date'])) {
            $value = $value->where('created_at', ">=", $input['start_date']);
        }

        if (isset($input['latitude'])) {
            $value = $value->where('latitude', $input['latitude']);
        }
        if (isset($input['longitude'])) {
            $value = $value->where('longitude', $input['longitude']);
        }

    }

    /**
     * getDetails => get details for listing
     *
     * @param  mixed $input
     *
     * @return void
     */
    public function getDetails($input = null)
    {
        $value = $this->makeModel();

        // common filter applied here
        $this->commonFilterFn($value, $input);

        $count = $value->count();

        return [
            'count' => $count,
            'list' => $value
        ];
    }
}
